
import '../App.css';

import About from './About';
import {
  useParams,
} from "react-router-dom";

function Javascript() {
  let { id } = useParams();
 

  return (
    <div className="App">
      <header className="App-header">
        <h3>File: {id}</h3>
      
          <About 
            id={id}
            name={'Chandra'}
          />
      </header>
    </div>
  );
}

export default Javascript;
