
import React, { Component } from 'react';

import Language from '../components/LanguageComponent';


class About extends Component {

  languageList = {
    list: [
      {
        name: 'HTML & CSS',
        id: 'HTML & CSS',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg'
      },
      {
        name: 'JavaScript',
        id: 'JavaScript',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/es6.svg'
      },
      {
        name: 'React',
        id: 'React',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/react.svg'
      },
      {
        name: 'Ruby',
        id: 'Ruby',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/ruby.svg'
      },
      {
        name: 'Ruby on Rails',
        id: 'Ruby on Rails',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/rails.svg'
      },
      {
        name: 'Python',
        id: 'Python',
        image: 'https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/python.svg'
      }
    ]
  };

  listLanguage = (language) => {
    return (
      language.map((languageItem) => {
        if (languageItem.name === this.props.id) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={false}
            />
          )
        } else if (this.props.id === undefined) {
          return (
            <Language
              name={languageItem.name}
              image={languageItem.image}
              isBtn={true}
            />
          )
        }
      
      })
    )
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
         
          <p>
        Ini merupakan halaman About
          </p>
          {this.listLanguage(this.languageList.list)}
        </header>
      </div>
    );
  }
}

export default About;
