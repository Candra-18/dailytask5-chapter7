import '../App.css';

function Language() {
    return (
        
        <div className='language-item'>
            <div className='language-name'>Belajar React router</div>
            <img className='language-image' src='https://s3-ap-northeast-1.amazonaws.com/progate/shared/images/lesson/react/html.svg' />
        </div>
    );
}

export default Language;