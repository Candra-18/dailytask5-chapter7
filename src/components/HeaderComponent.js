import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";



function Header() {
  return (
    <header>


    <nav class="navbar navbar-expand-lg navbar-light bg-light pt-4">
      <div class="container">
        <a class="navbar-brand" href="#">
     
        </a>
        <div class="collapse navbar-collapse">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Binar Car Rental</a>
            </li>
          </ul>
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end bg-light" id="navbarNav">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link me-2" href="#our-services">Our Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link me-2" href="#why-us">Why Us</a>
            </li>
            <li class="nav-item">
              <a class="nav-link me-2" href="#testi">Testimonial</a>
            </li>
            <li class="nav-item">
              <a class="nav-link me-2" href="#faq">FAQ</a>
            </li>
            <li class="nav-item">
              <button class="btn btn-primary btn-success-register p-0">Register</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>


  </header>
  
)}

export default Header;