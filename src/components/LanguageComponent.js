import React, { Component } from 'react';
import '../App.css';
import {
  Link,
} from "react-router-dom";


class Language extends Component {
  render() {
    

    return (
      

      <div className='language-item'>
        <div className='language-name'>{this.props.name}</div>
        <img
          className='language-image'
          src={this.props.image}
        />
        {this.props.isBtn ? (
          <Link to={{
            pathname: `/language/${this.props.name}`,
          }}
          >
            <button>Button</button>
          </Link>
        ) : (<></>)}
      </div>
    );
  }
}

export default Language;